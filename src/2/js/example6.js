var app6 = angular.module('app6', []);

// If you use a controller twice on one page each controller has its own $scope. 
// You can update the $scopes however by using the $rootScope.
app6.controller('heroCtrl', function ($scope, $rootScope) {
    $scope.hero = [
        { realName: "Bruce Wayne", heroName: "Batman" },
        { realName: "Clark Kent", heroName: "Superman" }
    ];

    // now need to define the function get heroData
    $scope.getHeroData = function () {

        // search through the array of super heros
        heroSearch($scope.heroName);
    };

    // Searches through the hero array for a match
    function heroSearch(name) {

        // set default
        $scope.heroData = "Not Found";

        // If a hero is found it is returned
        for (var i = 0; i < $scope.hero.length; i++) {
            if($scope.hero[i].heroName === name){
                $scope.heroData = $scope.hero[i].realName + " is " + $scope.hero[i].heroName;
            }
        }
    }

    // If a broadcast is caught named heroUpdated the new heroUpdated is added to the other controllers $scope
    // update the $scope in both controllers
    // when there is a broadcast called heroUpdated
    $scope.$on("heroUpdated", function(event, args){
        $scope.hero.push({
            realName: args.realName, heroName: args.heroName
        });
    });

    // When a new hero is added we broadcast the update to the other controllers $scope
    $scope.addHeroData = function(realName, heroName){

        // broadcast the change that we have a new hero being added
        // call this broadcast hero updated
        // the information passing into this { }
        $rootScope.$broadcast("heroUpdated", 
        {
            realName: realName, heroName: heroName
        });

        // check if any error comes through
        console.log("Real : " + realName + ",  Hero : " + heroName);
    };
    // now need a way to catch this broadcast (positioned top of this function)

});
