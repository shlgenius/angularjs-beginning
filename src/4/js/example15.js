var app15 = angular.module('app15', []);

app15.controller('mainCtrl', function ($scope, $http) {

    // Get data from the json file and display it
    $scope.getData = function () {
        $http.get("playerData.json").success(
            function (data) {
                $scope.players = data;
            }
        );
    }


    // Get this Chrome extension if you get the CORS error
    // Allow-Control-Allow-Origin extension
    $scope.translate = function () {

        // get all the words input over in the html
        // then take words to translate and replace all the spaces in them to plus signs
        var words = $scope.wordsToTranslate.replace(/ /g, "+");

        // provide a link to the url (web service)
        var jsonUrl = "http://newjustin.com/translateit.php?action=translations&english_words=" + words;

        // now to use the http service pass in jsonUrl to get the http service 
        $http.get(jsonUrl).success(

            // function will print all the different translation out on the screen
            function (data) {
                $scope.translated = data;
            }
        );

    }

});