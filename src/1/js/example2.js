var app2 = angular.module('app2', []);

app2.controller('ctrl1', function($scope) {
    $scope.randomNumber1 = Math.floor((Math.random() * 10) + 1);
    $scope.randomNumber2 = Math.floor((Math.random() * 10) + 1);
});

// Define multiple controllers
app2.controller('badMoodCtrl', function($scope) {

    // create an array of bad feelings
    var badFeelings = ["Disregarded", "Unimportant", "Rejected", "Powerless"];
    $scope.bad = badFeelings[Math.floor((Math.random() * 4))];
    
});

app2.controller('goodMoodCtrl', function($scope) {

    // create an array of good feelings
    var goodFeelings = ["Pleasure", "Awesome", "Lovable", "Inner Peace"];
    $scope.good = goodFeelings[Math.floor((Math.random() * 4))];
    
});