var app12 = angular.module('app12', []);

// When we use a template the template replaces the content in the document. 
// You can use transclusion to display the original content and add in the new.

app12.directive("exDir", function() {
    return {
        transclude: true,

        // ng-transclude defines where the data in the element shows up in the template
        // define the template and where exactly we want our information that is already on the html 
        // where this Lorem ipsum to show up inside of our template
        // because we are not replacing it we are just going to put additional information inside of it with it
        template: "<div><h4>{{moreLorem}}</h4></div><div ng-transclude></div>"
    }
});

app12.controller("mainCtrl", function($scope){
    $scope.moreLorem = "The Amazing Lorem Story";
});

